---
# Color Definitions.
color:
    - name:
      type:
      value:

# Color Settings.
main-color:
certificate-color:
name-color:
organizer-color:

# Main Content.
language:
certificate:
certifies:
has-attended:
held:
at:
organizer-introduction:

# Informations about the Attendant.
name:

# Informations about the Event.
event:
date:
building:
city:
state:
country:
logo:
    -

# Informations about the Organizer.
organizer:
    name:
    signature:
    institute:
        -
---
