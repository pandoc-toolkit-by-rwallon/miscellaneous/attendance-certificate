---
# Color Settings.
main-color: Cerulean
certificate-color: MidnightBlue
name-color: RoyalBlue
organizer-color: RoyalBlue

# Informations about the Attendant.
name: Diana Campbell

# Informations about the Event.
event: Release 0.1.0
date: December 27, 2019
building: Pandoc Toolkit
city: Markdown City
state: State of LaTeX
country: PANDOC
logo:
    - images/latex.png
    - images/markdown.png

# Informations about the Organizer.
organizer:
    name: Eric Schneider
    institute:
        - Pandoc University
        - LaTeX Institute
---
