# Use Cases of the *Attendance Certificate* Template

This directory contains examples illustrating different use cases of the Pandoc
template for creating attendance certificates.

For each of the examples, you may either read its metadata file (to see how we
configured it) or download the final PDF.

## English Certificate

This example uses the default content for the certificate, and adds logos
to the header of the certificate.

The signature is to be added by the organizer.

*Metadata file available [here](english.md).*

*PDF available [here](/../builds/artifacts/master/file/english.pdf?job=make-example).*

## French Certificate

This example illustrates how the content of the certificate may be translated,
and does not use any logo.

The signature of the organizer is already added.

*Metadata file available [here](french.md).*

*PDF available [here](/../builds/artifacts/master/file/french.pdf?job=make-example).*
