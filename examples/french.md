---
# Color Settings.
main-color: SeaGreen
certificate-color: PineGreen
name-color: ForestGreen
organizer-color: ForestGreen

# Main Content.
language: francais
certificate: CERTIFICAT
certifies: Ce document certifie que
has-attended: a assisté à la
held: qui s'est tenue le
at: pour la
organizer-introduction: Au nom du comité d'organisation

# Informations about the Attendant.
name: Catherine Dupont

# Informations about the Event.
event: Sortie de la Version 0.1.0
date: 27 décembre 2019
building: Boîte à Outil Pandoc
city: Markdown Ville
state: État du LaTeX
country: PANDOC

# Informations about the Organizer.
organizer:
    name: Martin Durand
    signature: images/durand.png
    institute:
        - Université de Pandoc
        - Institut LaTeX
---
