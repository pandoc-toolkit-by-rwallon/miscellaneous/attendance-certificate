# Changelog

This file describes the evolution of the *Attendance Certificate* LaTeX
template for Pandoc.

Note that versions are numbered using the `BREAKING.FEATURE.FIX` scheme.

## Version 0.1.0 (December 2019)

+ The main content of the certificate may be adapted from YAML.
+ Colors may be customized from YAML.
+ Logos may be set from YAML.
+ The informations about the event are set from YAML.
+ The name of the attendant is set from YAML.
+ The informations about the organizer are set from YAML.
+ The signature of the organizer may be set from YAML.
