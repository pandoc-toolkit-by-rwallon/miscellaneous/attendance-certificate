# Pandoc Template for *Attendance Certificates*

[![pipeline status](https://gitlab.com/pandoc-toolkit-by-rwallon/miscellaneous/attendance-certificate/badges/master/pipeline.svg)](https://gitlab.com/pandoc-toolkit-by-rwallon/miscellaneous/attendance-certificate/commits/master)

## Description

This project provides a template allowing an easy creation of attendance
certificates with [Pandoc](https://pandoc.org).

This template is distributed under a Creative Commons Attribution 4.0
International License.

[![CC-BY 4.0](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/)

## Requirements

To build your certificate using this template, [Pandoc](https://pandoc.org)
at least 2.0 have to be installed on your computer.

## Creating your Attendance Certificate

To customize the attendance certificate to your own use case, you need to setup
the YAML configuration.
For convenience, we provide a [`metadata.md`](./metadata.md) file in which all
possible settings are specified, so that you can just set them or remove the
ones which do not fit your needs.
Read below for details on how to configure your attendance certificate, and see
our various [examples](examples).

### Colors

#### Color Definitions

You may define your own colors through the variable `color`, which must be
set to a list of objects defining three fields:

+ `name`: the name of the color you are defining.
+ `type`: the type of the color, among `gray`, `rgb`, `RGB`, `html`, `HTML`,
  etc.
+ `value`: the actual value of the color.

Note that each color is then automatically translated into LaTeX by using the
following command:

```latex
\definecolor{name}{type}{value}
```

#### Color Settings

Once you have defined your own colors, you may set the following properties
to customize the colors of your certificate (of course, you may also use
predefined colors):

+ `main-color`: the color for the main text of the certificate.
+ `certificate-color`: the color for the title of the certificate.
+ `name-color`: the color for the name of the attendant.
+ `organizer-color`: the color for the name and institute(s) of the organizer.

By default, all these colors are set to `black`.

### Main Content

You may customize the content of the certificate by setting the following
variables to the text you want to display on it.

+ `certificate`: the title of the certificate.
+ `certifies`: a short sentence introducing what the certificate certifies
  about the attendant.
+ `has-attended`: a verb introducing the name of the attended event.
+ `held`: a verb specifying when the event took place.
+ `at`: a preposition introducing the location of the event.
+ `organizer-introduction`: a short sentence introducing the signature of the
  organizer.

These variables are particularly useful for writing certificates in different
languages.
In this case, you may also set `language` to the language in which you are
writing your certificates.

### Informations about the Attendant.

You can specify the `name` of the attendant by setting the corresponding
variable.

### Informations about the Event.

The variables `event` and `date` specify the name of the event and when it took
place, respectively.

You may also specify several informations about the location of the event,
namely the `building`, `city`, `state` and `country` in which it took place.
Only `city` and `country` are required.

You may also add the logos of the event within the list `logo`.
If no logo is specified, the name of the event is put instead.

### Informations about the Organizer.

The variable `organizer` defines three fields used to provide informations
about the organizer of the event.

+ `name`: the name of the organizer.
+ `signature`: the path to the signature of the organizer (optional, a blank
  space is left for a hand-written signature if not specified).
+ `institute`: the list of the institutes of the organizer.

## Building your Attendance Certificate

Suppose that you have written the metadata of the attendance certificate in a
file `input.md`.
Then, to produce the certificate in a file named `output.pdf`, execute the
following command:

```bash
pandoc --template attendance-certificate.pandoc -o output.pdf input.md
```
